﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using BlimpStore.Domain.Models;

namespace BlimpStore.DataAccess
{
    internal class BlimpStoreInitializer : DropCreateDatabaseAlways<BlimpStoreContext>
    {
        protected override void Seed(BlimpStoreContext context)
        {
            // Initial products
            var arm1 = new Product("Titan-X1 (ARM)", 1199, "/Content/Images/bionic-arm2.jpg", new DateTime(2010, 06, 12));
            var arm2 = new Product("Titan-X2 (ARM)", 899, "/Content/Images/bionic-arm.png", new DateTime(2014, 03, 11));
            var arm3 = new Product("Titan-X3 (ARM)", 1399, "/Content/Images/bionic-arm3.png", new DateTime(2016, 09, 13));
            var hand1 = new Product("BioCr v1 (HAND)", 599, "/Content/Images/bionic-hand2.png", new DateTime(1995, 01, 02));
            var hand2 = new Product("RazorTX v1 (HAND)", 699, "/Content/Images/bionic-hand3.png", new DateTime(2014, 07, 23));
            var hand3 = new Product("RazorTX v2 (HAND)", 699, "/Content/Images/bionic-hand4.jpg", new DateTime(2010, 07, 01));
            var leg1 = new Product("HGJump M10 (LEG)", 999, "/Content/Images/bionic-leg.png", new DateTime(2001, 07, 01));
            var leg2 = new Product("BioWalk One (LEG)", 799, "/Content/Images/bionic-leg3.jpg", new DateTime(2004, 09, 12));
            var leg3 = new Product("BioWalk Two (LEG)", 1299, "/Content/Images/bionic-leg4.jpg", new DateTime(2014, 03, 28));
            var leg4 = new Product("HGJump M50 (LEG)", 1499, "/Content/Images/bionic-leg5.jpg", new DateTime(2015, 02, 15));

            var productList = new List<Product> { arm1, arm2, arm3, hand1, hand2, hand3, leg1, leg2, leg3, leg4 };
            productList.ForEach(p => context.Products.AddOrUpdate(p));
            context.SaveChanges();

            // Initial cart
            var cart1 = new Cart();
            context.Carts.AddOrUpdate(cart1);
            context.SaveChanges();
        }
    }
}