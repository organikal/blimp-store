﻿using System.Data.Entity;
using BlimpStore.Domain.Models;

namespace BlimpStore.DataAccess
{
    public class BlimpStoreContext : DbContext
    {
        public BlimpStoreContext() : base()
        {
            Database.SetInitializer(new BlimpStoreInitializer());
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
    }
}
