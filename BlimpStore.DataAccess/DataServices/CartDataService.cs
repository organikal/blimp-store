﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using BlimpStore.Domain.Models;

namespace BlimpStore.DataAccess.DataServices
{
    public class CartDataService
    {
        public Cart Get(int id)
        {
            using (var context = new BlimpStoreContext())
            {
                return context.Carts.Include("CartItems").Where(c => c.Id == id).FirstOrDefault<Cart>();
            }
        }

        public List<Cart> GetAll()
        {
            using (var context = new BlimpStoreContext())
            {
                return context.Carts.ToList();
            }
        }

        public void RemoveItemFromCart(int id)
        {
            using (var context = new BlimpStoreContext())
            {
                // get the cart
                var cart = context.Carts.Include("CartItems").SingleOrDefault(c => c.Id == 1);
                // get the item
                var itemToFind = cart.CartItems.FirstOrDefault(item => item.Product.Id == id);

                context.CartItems.Attach(itemToFind);
                context.CartItems.Remove(itemToFind);
                context.SaveChanges();
            }
        }

        public void AddItemToCart(Product product)
        {
            using (var context = new BlimpStoreContext())
            {
                var cart = context.Carts.Include("CartItems").SingleOrDefault(c => c.Id == 1);
                
                // we check if the product was already in the cart
                var itemToFind = cart.CartItems.FirstOrDefault(item => item.Product.Id == product.Id);

                // it's not in the cart
                if (itemToFind == null)
                {
                    var productInDb = context.Products.FirstOrDefault(p => p.Id == product.Id);

                    CartItem newItem = new CartItem(productInDb, cart);   
                    cart.CartItems.Add(newItem);
                    context.SaveChanges();
                }

                // it was in the cart already
                else
                {
                    itemToFind.Quantity += 1;
                    context.SaveChanges();
                }
            }
        }



        // + 1
        public void IncrementItemInCart(int id)
        {
            using (var context = new BlimpStoreContext())
            {
                var cart = context.Carts.Include("CartItems").SingleOrDefault(c => c.Id == 1);  // get the cart
                var itemToFind = cart.CartItems.FirstOrDefault(item => item.Product.Id == id);  // get the item
                itemToFind.Quantity += 1;   // +1
                context.SaveChanges();      // save changes
            }
        }



        // - 1
        public bool DecrementItemInCart(int id)
        {
            using (var context = new BlimpStoreContext())
            {
                var cart = context.Carts.Include("CartItems").SingleOrDefault(c => c.Id == 1);  // get the cart
                var itemToFind = cart.CartItems.FirstOrDefault(item => item.Product.Id == id);  // get the item
                itemToFind.Quantity -= 1;   // -1
                context.SaveChanges();      // save changes

                // time to delete it
                if (itemToFind.Quantity == 0)
                    return true;
                else
                {
                    return false;
                }
            }
        }

        public void Add(Cart cart)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Carts.AddOrUpdate(cart);
                context.SaveChanges();
            }
        }

        public void Update(Cart cart)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Carts.AddOrUpdate(cart);
                context.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Carts.Remove(context.Carts.Where(s => s.Id == Id).FirstOrDefault<Cart>());
                context.SaveChanges();
            }
        }
    }
}
