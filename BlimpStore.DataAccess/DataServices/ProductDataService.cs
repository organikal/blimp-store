﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using BlimpStore.Domain.Models;

namespace BlimpStore.DataAccess.DataServices
{
    public class ProductDataService
    {
        public Product Get(int id)
        {
            using (var context = new BlimpStoreContext())
            {
                return context.Products.Where(a => a.Id == id).FirstOrDefault<Product>();
            }
        }

        public List<Product> GetAll()
        {
            using (var context = new BlimpStoreContext())
            {
                return context.Products.ToList();
            }
        }

        public void Add(Product country)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Products.AddOrUpdate(country);
                context.SaveChanges();
            }
        }

        public void Update(Product article)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Products.AddOrUpdate(article);
                context.SaveChanges();
            }
        }

        public void Remove(int Id)
        {
            using (var context = new BlimpStoreContext())
            {
                context.Products.Remove(context.Products.Where(s => s.Id == Id).FirstOrDefault<Product>());
                context.SaveChanges();
            }
        }

        public List<Product> GetLatestProducts(int number)
        {
            using (var context = new BlimpStoreContext())
            {
                return context.Products.Take(number).OrderByDescending(a => a.Updated).ToList();
            }

        }
    }
}
