﻿using BlimpStore.Domain.Models;

namespace BlimpStoreWeb.Models
{
    public class CartItemViewModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public virtual ProductViewModel Product { get; set; }
        public virtual CartViewModel Cart { get; set; }

        public CartItemViewModel() { }
    }
}