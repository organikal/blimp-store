﻿using System.Collections.Generic;

namespace BlimpStoreWeb.Models
{
    public class CartViewModel
    {
        public int Id { get; set; }

        public virtual ICollection<CartItemViewModel> CartItems { get; set; }

        public CartViewModel()
        {
            CartItems = new List<CartItemViewModel>();
        }

        public CartViewModel(CartItemViewModel cartItem)
        {
            CartItems = new List<CartItemViewModel>();
            CartItems.Add(cartItem);
        }
    }
}