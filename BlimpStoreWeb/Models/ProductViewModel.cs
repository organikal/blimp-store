﻿using System;
using System.Collections.Generic;
using BlimpStore.Domain.Models;

namespace BlimpStoreWeb.Models
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string Image { get; set; }

        public DateTime Updated { get; set; }

        public virtual ICollection<CartItemViewModel> CartItems { get; set; }

        public ProductViewModel () { }

        public ProductViewModel(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Price = product.Price;
            Image = product.Image;
            Updated = product.Updated;
        }
    }
}