﻿namespace BlimpStoreWeb.Models
{
    public class HomeApiFilter
    {
        public int Amount { get; set; }
        public bool TakeLatest { get; set; }

        public HomeApiFilter()
        {
            Amount = int.MaxValue;
        }
    }
}