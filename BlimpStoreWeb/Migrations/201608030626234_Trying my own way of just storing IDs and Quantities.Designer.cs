// <auto-generated />
namespace BlimpStoreWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class TryingmyownwayofjuststoringIDsandQuantities : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TryingmyownwayofjuststoringIDsandQuantities));
        
        string IMigrationMetadata.Id
        {
            get { return "201608030626234_Trying my own way of just storing IDs and Quantities"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
