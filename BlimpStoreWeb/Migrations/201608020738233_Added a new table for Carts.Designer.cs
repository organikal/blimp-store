// <auto-generated />
namespace BlimpStoreWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedanewtableforCarts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedanewtableforCarts));
        
        string IMigrationMetadata.Id
        {
            get { return "201608020738233_Added a new table for Carts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
