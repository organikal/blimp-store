﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlimpStoreWeb.Startup))]
namespace BlimpStoreWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
