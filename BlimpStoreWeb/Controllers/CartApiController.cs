﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BlimpStore.DataAccess.DataServices;

namespace BlimpStoreWeb.Controllers
{
    public class CartApiController : ApiController
    {
        private CartDataService CartManager = new CartDataService();

        [HttpPost]
        public void PostIncItem(int id)
        {
            CartManager.IncrementItemInCart(id);
        }

        [HttpDelete]
        public void DeleteDecItem(int id)
        {
            bool answer = CartManager.DecrementItemInCart(id);
            if (answer)
                CartManager.RemoveItemFromCart(id);
        }
    }
}
