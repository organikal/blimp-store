﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using BlimpStore.DataAccess.DataServices;

namespace BlimpStoreWeb.Controllers
{
    public class ProductController : Controller
    {
        private ProductDataService ProductManager = new ProductDataService();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(int id)
        {
            return View(id);
        }
    }
}
