﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BlimpStore.DataAccess;
using BlimpStore.DataAccess.DataServices;
using BlimpStore.Domain.Models;
using BlimpStoreWeb.Models;

namespace BlimpStoreWeb.Controllers
{
    [RoutePrefix("api/HomeApi")]
    public class HomeApiController : ApiController
    {
        private ProductDataService ProductManager = new ProductDataService();
        private CartDataService CartManager = new CartDataService();




        public List<ProductViewModel> Get([FromUri]HomeApiFilter filter)
        {
            if (filter == null)
            {
                filter = new HomeApiFilter();
            }

            var products = filter.TakeLatest
                ? ProductManager.GetLatestProducts(filter.Amount)
                : ProductManager.GetAll().Take(filter.Amount);

            var viewModel = products.ToList().Select(p => new ProductViewModel(p));

            return viewModel.ToList();
        }



        public ProductViewModel Get(int id)
        {
            // I get a product and convert it to ProductViewModel
            var product =  ProductManager.Get(id);

            var viewModelProduct = new ProductViewModel
            {
                Id = product.Id,
                Name = product.Name,
                Image = product.Image,
                Price = product.Price,
                Updated = product.Updated,
                CartItems = null,           // the ICollection
            };

            return viewModelProduct;
        }



        [HttpPost]
        public void AddToCart(Product product)
        {
            CartManager.AddItemToCart(product);
        }



        [HttpPut]
        public void Update(Product model)
        {
            ProductManager.Update(model);
        }




        [HttpDelete]
        public Object Delete(int id)
        {
            CartManager.RemoveItemFromCart(id);
            return null;
        }



        [Route("CartItems")]
        public List<CartItemViewModel> GetCartItems()
        {
            using (var context = new BlimpStoreContext())
            {
                var items = context.CartItems.Include("Product").ToList();

                var listViewModels = new List<CartItemViewModel>();

                foreach (var item in items)
                    listViewModels.Add(CartItemNormalToViewModel(item));

                return listViewModels;
            }
        }


//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2
        private CartItemViewModel CartItemNormalToViewModel(CartItem cartItem)
        {
            var viewModel = new CartItemViewModel();

            viewModel.Product = ProductNormalToViewModel(cartItem.Product);
            viewModel.Quantity = cartItem.Quantity;

            return viewModel;
        }



        private ProductViewModel ProductNormalToViewModel(Product product)
        {
            var viewModel = new ProductViewModel();

            viewModel.Id = product.Id;
            viewModel.Name = product.Name;
            viewModel.Image = product.Image;
            viewModel.Price = product.Price;
            viewModel.Updated = product.Updated;

            return viewModel;
        }
    }
}
