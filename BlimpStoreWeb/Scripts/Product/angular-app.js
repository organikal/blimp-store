var ProductAngularApp;
(function (ProductAngularApp) {
    angular.module('product-app', ['ngResource'])
        .service('productService', ['$resource', ProductAngularApp.Services.ProductService])
        .directive('myEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.myEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    })
        .controller('productListController', ['$resource', 'productService', ProductAngularApp.Controllers.ProductListController])
        .controller("productDetailsController", ['$resource', 'productService', ProductAngularApp.Controllers.ProductDetailsController])
        .controller("cartItemsController", ['$resource', 'productService', ProductAngularApp.Controllers.CartItemsController])
        .controller("cartCheckoutController", ['productService', ProductAngularApp.Controllers.CartCheckoutController]);
})(ProductAngularApp || (ProductAngularApp = {}));
//# sourceMappingURL=angular-app.js.map