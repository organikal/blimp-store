var ProductAngularApp;
(function (ProductAngularApp) {
    var Services;
    (function (Services) {
        var ProductService = (function () {
            function ProductService($resource) {
                this.$resource = $resource;
                this.repository = this.$resource("/api/HomeApi/:id", { id: "@id" });
            }
            // get all of the products
            ProductService.prototype.getProducts = function () {
                return this.repository.query();
            };
            // get the latest products
            ProductService.prototype.getLatest = function (amount) {
                if (amount != 0)
                    return this.repository.query({
                        Amount: "@amount",
                        TakeLatest: true
                    });
                else {
                    return this.repository.query();
                }
            };
            // get one product
            ProductService.prototype.getOneProduct = function (id) {
                return this.repository.get({ id: id });
            };
            // add product to cart
            ProductService.prototype.addItemToCart = function (item) {
                this.repository.save(item);
            };
            // +1 to quantity
            ProductService.prototype.incrementProductInCart = function (id) {
                var repository = this.$resource("/api/CartApi/:id", { id: "@id" });
                repository.save({ id: id });
            };
            // -1 to quantity
            ProductService.prototype.decrementProductInCart = function (id) {
                var repository = this.$resource("/api/CartApi/:id", { id: "@id" });
                repository.delete({ id: id });
            };
            // get cart items
            ProductService.prototype.getCartItems = function () {
                var cartRepository = this.$resource("/api/HomeApi/CartItems");
                return cartRepository.query();
            };
            // remove item from cart
            ProductService.prototype.removeFromCart = function (id) {
                //var cartRemoveRepository = this.$resource("/api/HomeApi");
                this.repository.delete({ id: id });
            };
            return ProductService;
        }());
        Services.ProductService = ProductService;
    })(Services = ProductAngularApp.Services || (ProductAngularApp.Services = {}));
})(ProductAngularApp || (ProductAngularApp = {}));
//# sourceMappingURL=product.service.js.map