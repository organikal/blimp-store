var ProductAngularApp;
(function (ProductAngularApp) {
    var Controllers;
    (function (Controllers) {
        var CartItemsController = (function () {
            function CartItemsController($resource, productService) {
                this.$resource = $resource;
                this.productService = productService;
                this.getCartItems();
            }
            CartItemsController.prototype.getCartItems = function () {
                this.cartItems = this.productService.getCartItems();
            };
            CartItemsController.prototype.removeItem = function (id) {
                this.productService.removeFromCart(id);
            };
            CartItemsController.prototype.incrementItem = function (id) {
                this.productService.incrementProductInCart(id);
            };
            CartItemsController.prototype.decrementItem = function (id) {
                this.productService.decrementProductInCart(id);
            };
            return CartItemsController;
        }());
        Controllers.CartItemsController = CartItemsController;
    })(Controllers = ProductAngularApp.Controllers || (ProductAngularApp.Controllers = {}));
})(ProductAngularApp || (ProductAngularApp = {}));
//# sourceMappingURL=cart-items.controller.js.map