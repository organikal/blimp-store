﻿module ProductAngularApp.Controllers {

    export class ProductDetailsController {

        public product: any;
        public cartItems: any;

        constructor(private $resource: angular.resource.IResourceService, private productService: Services.ProductService) {}

        public init(id: number) {
            this.product = this.productService.getOneProduct(id);
        }

        public addItem() {
            this.productService.addItemToCart(this.product);    // give item to the service
        }
    }
}