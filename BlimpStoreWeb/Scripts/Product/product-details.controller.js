var ProductAngularApp;
(function (ProductAngularApp) {
    var Controllers;
    (function (Controllers) {
        var ProductDetailsController = (function () {
            function ProductDetailsController($resource, productService) {
                this.$resource = $resource;
                this.productService = productService;
            }
            ProductDetailsController.prototype.init = function (id) {
                this.product = this.productService.getOneProduct(id);
            };
            ProductDetailsController.prototype.addItem = function () {
                this.productService.addItemToCart(this.product); // give item to the service
            };
            return ProductDetailsController;
        }());
        Controllers.ProductDetailsController = ProductDetailsController;
    })(Controllers = ProductAngularApp.Controllers || (ProductAngularApp.Controllers = {}));
})(ProductAngularApp || (ProductAngularApp = {}));
//# sourceMappingURL=product-details.controller.js.map