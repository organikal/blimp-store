var ProductAngularApp;
(function (ProductAngularApp) {
    var Controllers;
    (function (Controllers) {
        var ProductListController = (function () {
            function ProductListController($resource, productService) {
                this.$resource = $resource;
                this.productService = productService;
            }
            ProductListController.prototype.getAll = function () {
                debugger;
                this.allProducts = this.productService.getLatest(0);
            };
            ProductListController.prototype.getLatest = function () {
                this.latestProducts = this.productService.getLatest(5); // get latest products
            };
            ProductListController.prototype.addItem = function (product) {
                this.productService.addItemToCart(product); // give item to the service
            };
            ProductListController.prototype.filterSearch = function () {
                // Declare variables
                var input, filter, ul, li, a, i, myProduct;
                // Get the filter search text
                input = document.getElementById('myInput');
                // Convert it to upper case
                filter = input.value.toUpperCase();
                // I want to traverse it as an array
                ul = document.getElementById("the-row");
                // Grab the div that holds everything about my product
                myProduct = ul.getElementsByClassName("col-lg-4 text-center");
                // This tag contains the product name
                li = ul.getElementsByTagName('h4');
                // Loop through all list items, and hide those who don't match the search query
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName('a')[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        myProduct[i].style.display = '';
                    }
                    else {
                        myProduct[i].style.display = 'none';
                    }
                }
            };
            return ProductListController;
        }());
        Controllers.ProductListController = ProductListController;
    })(Controllers = ProductAngularApp.Controllers || (ProductAngularApp.Controllers = {}));
})(ProductAngularApp || (ProductAngularApp = {}));
//# sourceMappingURL=product-list.controller.js.map