﻿module ProductAngularApp {

    angular.module('product-app', ['ngResource'])
        .service('productService', ['$resource', Services.ProductService])
        .directive('myEnter', function ()
        {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.myEnter);
                    });

                    event.preventDefault();
                    }
                });
            };
        })
        .controller('productListController', ['$resource', 'productService', Controllers.ProductListController])
        .controller("productDetailsController", ['$resource', 'productService', Controllers.ProductDetailsController])
        .controller("cartItemsController", ['$resource', 'productService', Controllers.CartItemsController])
        .controller("cartCheckoutController", ['productService', Controllers.CartCheckoutController]);
}