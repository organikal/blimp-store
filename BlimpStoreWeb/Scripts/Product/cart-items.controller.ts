﻿module ProductAngularApp.Controllers {

    export class CartItemsController {

        public cartItems: any;

        constructor(private $resource: angular.resource.IResourceService, private productService: Services.ProductService) {
            this.getCartItems();
        }

        public getCartItems() {
            this.cartItems = this.productService.getCartItems();
        }

        public removeItem(id: number) {
            this.productService.removeFromCart(id);
        }

        public incrementItem(id: number) {
            this.productService.incrementProductInCart(id);
        }

        public decrementItem(id: number) {
            this.productService.decrementProductInCart(id);
        }
    }

}