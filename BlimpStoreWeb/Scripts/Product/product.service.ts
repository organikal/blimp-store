﻿module ProductAngularApp.Services {

    export class ProductService {

        private repository = this.$resource("/api/HomeApi/:id", { id: "@id" });
        
        
        private itemId: number;

        constructor(private $resource) {}


        // get all of the products
        public getProducts() {
            return this.repository.query();
        }

        // get the latest products
        public getLatest(amount: number)
        {
            if (amount != 0)
            return this.repository.query({
                Amount: "@amount",
                TakeLatest: true
                });

            else {
                return this.repository.query();
            }
        }

        // get one product
        public getOneProduct(id: number) {
            return this.repository.get({ id: id });
        }

        // add product to cart
        public addItemToCart(item: any) {
            this.repository.save(item);
        }

        // +1 to quantity
        public incrementProductInCart(id: number) {
            var repository = this.$resource("/api/CartApi/:id", { id: "@id" });
            repository.save({ id: id });
        }

        // -1 to quantity
        public decrementProductInCart(id: number) {
            var repository = this.$resource("/api/CartApi/:id", { id: "@id" });
            repository.delete({ id: id });
        }

        // get cart items
        public getCartItems() {
            var cartRepository = this.$resource("/api/HomeApi/CartItems");
            return cartRepository.query();
        }

        // remove item from cart
        public removeFromCart(id: number) {
            //var cartRemoveRepository = this.$resource("/api/HomeApi");
            this.repository.delete({ id: id });
        }
    }
}