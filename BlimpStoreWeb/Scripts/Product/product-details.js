var AngularApp;
(function (AngularApp) {
    var Controllers;
    (function (Controllers) {
        var ArticlesDetailsController = (function () {
            function ArticlesDetailsController($resource, $routeParams, $location) {
                this.$resource = $resource;
                this.$routeParams = $routeParams;
                this.$location = $location;
                this.repository = this.$resource("/api/Articles/:id", { id: "@id" });
                this.btnEditForm = false;
                this.article = this.getArticle($routeParams.id);
            }
            // get article by id
            ArticlesDetailsController.prototype.getArticle = function (id) {
                var _this = this;
                this.repository.get({ id: id }, function (result) {
                    _this.article = result;
                }, function () {
                    _this.$location.path('/Article');
                });
            };
            ArticlesDetailsController.prototype.showSubmitForm = function () {
                this.btnEditForm = true;
            };
            ArticlesDetailsController.prototype.saveArticle = function (article) {
                var _this = this;
                //this.btnEditForm = false;
                this.repository.save(article, function () {
                    _this.$location.path('/Article');
                });
            };
            ArticlesDetailsController.prototype.deleteArticle = function (ID) {
                var _this = this;
                this.repository.delete({ id: ID }, function () {
                    _this.$location.path('/Article');
                });
            };
            ;
            return ArticlesDetailsController;
        }());
        Controllers.ArticlesDetailsController = ArticlesDetailsController;
    })(Controllers = AngularApp.Controllers || (AngularApp.Controllers = {}));
})(AngularApp || (AngularApp = {}));
