﻿module ProductAngularApp.Controllers {

    export class ProductListController {
        public latestProducts: any;
        public allProducts: any;

        constructor(private $resource: angular.resource.IResourceService, private productService: Services.ProductService) {}

        public getAll() {
            debugger;
            this.allProducts = this.productService.getLatest(0);
        }

        public getLatest() {
            this.latestProducts = this.productService.getLatest(5);       // get latest products
        }

        public addItem(product: any) {
            this.productService.addItemToCart(product);    // give item to the service
        }

        public filterSearch() {
            // Declare variables
            var input, filter, ul, li, a, i, myProduct;

            // Get the filter search text
            input = document.getElementById('myInput');

            // Convert it to upper case
            filter = input.value.toUpperCase();

            // I want to traverse it as an array
            ul = document.getElementById("the-row");

            // Grab the div that holds everything about my product
            myProduct = ul.getElementsByClassName("col-lg-4 text-center");

            // This tag contains the product name
            li = ul.getElementsByTagName('h4');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName('a')[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    myProduct[i].style.display = '';
                } else {
                    myProduct[i].style.display = 'none';
                }
            }
        }
    }
}