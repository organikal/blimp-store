﻿namespace BlimpStore.Domain.Models
{
    public class CartItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public virtual Product Product { get; set; }
        public virtual Cart Cart { get; set; }

        public CartItem() { }

        public CartItem(Product product, Cart cart)
        {
            Quantity = 1;
            Product = product;
            Cart = cart;
        }
    }
}