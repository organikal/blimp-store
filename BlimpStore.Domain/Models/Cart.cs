﻿using System.Collections.Generic;

namespace BlimpStore.Domain.Models
{
    public class Cart
    {
        public int Id { get; set; }

        public virtual ICollection<CartItem> CartItems { get; set; }

        public Cart()
        {
            CartItems = new List<CartItem>();
        }

        public Cart(CartItem cartItem)
        {
            CartItems = new List<CartItem>();
            CartItems.Add(cartItem);
        }
    }
}
