﻿using System;
using System.Collections.Generic;


namespace BlimpStore.Domain.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string Image { get; set; }

        public DateTime Updated { get; set; }

        public virtual ICollection<CartItem> CartItems { get; set; }

        public Product()
        {
            CartItems = null;
        }

        public Product(string name, int price, string image, DateTime updated)
        {
            Name = name;
            Price = price;
            Image = image;
            Updated = updated;
            CartItems = null;
        }
    }
}
